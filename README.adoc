= Massgainer

== Motivation

Apply ReplayGain tags to all music files using the command line.


== What it does

This script scans all music files of supported formats under the given
directories, and apply appropriate ReplayGain tags to them using external tools.
Currently, the following file extensions are scanned:

* `aif`
* `aiff`
* `alac`
* `ape`
* `asf`
* `flac`
* `m4a`
* `mp2`
* `mp3`
* `mp4`
* `oga`
* `ogg`
* `opus`
* `spx`
* `wav`
* `wma`
* `wv`


== Prerequisites

* Bash
** awk
** find
** grep
** tee
** wc
* https://github.com/Moonbase59/loudgain[loudgain^]


== Installation

[source,console]
----
> git clone ... ...
----

== Usage

=== Env vars

`PARALLEL`::
  optional.  Default = `1` (for "true")

`PARALLEL_WAIT_PERIOD`::
  optional.  Default = `5` seconds


=== Options

`-h`::
  display usage info

`-V`::
  display version

`-n`::
  dry run

`-d`::
  debug mode (implies verbose mode)

`-v`::
  verbose mode


[[advanced_options]]
=== Advanced options

`-p`::
  (default) Run jobs in parallel

`-P`::
  Do not run jobs in parallel

`-w`::
  Retry period (in seconds) for when number of processes exceeds threshold
  (default: 5)

=== Examples

Tag everything:

[source,console]
----
$ ./massgainer .
----

== Licence

The MIT License.  See link:LICENSE[`LICENSE`^].
