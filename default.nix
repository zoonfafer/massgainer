{ pkgs ? import <nixpkgs> { }
, lib ? pkgs.lib
, stdenv ? pkgs.stdenv
}:

stdenv.mkDerivation rec {
  pname = "massgainer";
  version = "unstable";

  src = ./.;

  buildInputs = with pkgs; [
    bash
    loudgain
    coreutils
  ];

  nativeBuildInputs = with pkgs; [ makeWrapper ];

  installPhase = with pkgs; ''
    mkdir -p $out/bin
    cp bin/massgainer $out/bin/
    wrapProgram $out/bin/massgainer \
    --prefix PATH : ${lib.makeBinPath [ bash loudgain coreutils ]}
  '';

}
